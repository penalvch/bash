#!/bin/bash
# Tested on Ubuntu 22.04.3 LTS

# CPU %
cpuidle=$(vmstat 1 2 | tail -1 | awk '{print $15}')
cpuidle=$(echo "scale=2;100-$cpuidle" | bc)
cpuhigh=90
cpuresult=$(echo "$cpuidle > $cpuhigh" | bc)
if["$cpuresult" -eq 1 ]; then
    # action
fi

# Disk %
diskused=$(df / | awk 'NR==2 {print $5}')
diskused="${diskused%?}"
diskhigh=90
diskresult=$(echo "$diskused > $diskhigh" | bc)
if["$diskresult" -eq 1 ]; then
    # action
fi

# Memory %
memline=$(free | grep Mem)
memtotal=$(echo $memline | awk '{print $2}')
memused=$(echo $memline | awk '{print $3}')
memperc=$(echo "scale=2; $memused / $memtotal * 100" | bc)
memhigh=90
memresult=$(echo "$memperc > $memhigh" | bc)
if["$memresult" -eq 1 ]; then
    # action
fi

# dmesg check for Call Trace
call="] Call Trace:"
dmesgvar=$(dmesg)
if[[ "$dmesgvar" == *"$call"* ]]; then
    # action
fi

# Zombie process check
zombiechk=$(ps aux | awk '{print $8}' | grep -w Z | wc -l)
if [ $zombiechk -gt 0 ]; then
    # action
fi

# Updates need to be installed
upcheck=$(apt-get -s upgrade)
upstrings=("The following packages will be upgraded:" "The following packages have been kept back:")
for item in "${upstrings[@]}"; do
    if [[ "$upcheck" == *"$item"* ]]; then
        # action
    fi
done

# Apport crash files exist https://wiki.ubuntu.com/Apport
apportcheck="/var/crash"
if [ "$(ls -A $apportcheck)" ]; then
    # action
fi

# Packages installed check
packages=("snmp" "adsys")
for item in "${packages[@]}"; do
    temp=$(apt-cache policy $item)
    length=${#temp}
    if [[ $temp == *"Installed: (none)"* ]]; then
        # action
    elif [[ $length == 0 ]]; then
        # If "Unable to locate package" the string variable length is 0 by default
        # action
    fi
done

# Kernel module installed check
modchk=$(modinfo modname 2>&1 >/dev/null)
if [[ -n "$modchk" ]]; then
    # Received error message:
    # modinfo: ERROR: Module alias modname not found.
fi
